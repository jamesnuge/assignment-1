package xyz.jamesnuge.scheduling.qf;

import fj.data.Either;
import fj.data.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import xyz.jamesnuge.Pair;
import xyz.jamesnuge.config.SystemConfig;
import xyz.jamesnuge.messaging.ClientMessagingService;
import xyz.jamesnuge.messaging.JobQueryResult;
import xyz.jamesnuge.messaging.NewJobRequest;
import xyz.jamesnuge.scheduling.StateMachine;
import xyz.jamesnuge.scheduling.quickFit.QfFactory;
import xyz.jamesnuge.scheduling.quickFit.QfInternalState;
import xyz.jamesnuge.state.ServerStateItem;

import java.util.HashMap;
import java.util.Map;

import static fj.data.Either.right;
import static fj.data.List.list;
import static fj.data.List.nil;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static xyz.jamesnuge.MessageParser.Message.OK;
import static xyz.jamesnuge.Util.flatMap;
import static xyz.jamesnuge.Util.mapOf;
import static xyz.jamesnuge.fixtures.ServerStateItemFixtures.generateServerStateItem;
import static xyz.jamesnuge.fixtures.ServerStateItemFixtures.generateServerStateItemWithNoJobs;
import static xyz.jamesnuge.util.TestUtil.assertRight;

public class QfFactoryTest {

    private static final Either<String, String> WRITE_RESULT = right("write");

    @Mock
    ClientMessagingService cms;

    @Mock
    SystemConfig systemConfig;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void shouldPerformNoopIfTriggerUnknown() {
        Either<String, QfInternalState> currentState = performActionWithInitialState("Unknown action");
        assertRight(
                new QfInternalState(false, systemConfig, new HashMap<>(), new HashMap<>()),
                currentState
        );
    }

    @SuppressWarnings("unchecked")
    public void testStateMachineAssignsJobToTheBestFitMachineWithoutJobs() {
        final List<ServerStateItem> config = list(
                generateServerStateItemWithNoJobs("large", 3),
                generateServerStateItemWithNoJobs("small", 1),
                generateServerStateItemWithNoJobs("medium", 2)
        );
        when(cms.getServerState()).thenReturn(right(config));
        when(cms.scheduleJob(any(), any(), any())).thenReturn(WRITE_RESULT);
        when(cms.getMessage()).thenReturn(right(OK.name()), right(""));
        when(cms.signalRedy()).thenReturn(WRITE_RESULT);
        when(cms.getServerState(any())).thenReturn(right(config));
        Either<String, QfInternalState> currentState = performAction(
                "JOBN 2142 12 750 3 250 800",
                new QfInternalState(false, systemConfig, new HashMap<>(), new HashMap<>())
        );
        NewJobRequest expectedJobRequest = new NewJobRequest(12, 2142, 750L, 3, 250L, 800L);
        assertRight(
                new QfInternalState(false, systemConfig, mapOf(12, expectedJobRequest), mapOf(12, Pair.of("large", 3))),
                currentState
        );
        verify(cms).scheduleJob(eq(12), eq("large"), eq(3));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testStateMachineAssignsJobToMostCapableServerIfThereAreNoAvailableServers() {
        final List<ServerStateItem> config = list(
                generateServerStateItem("Unavailable but capable", 3, 1, 1)
        );
        when(cms.getServerState(any())).thenReturn(right(nil()), right(config));
        when(cms.scheduleJob(any(), any(), any())).thenReturn(WRITE_RESULT);
        when(cms.getMessage()).thenReturn(right(OK.name()), right(""));
        when(cms.signalRedy()).thenReturn(WRITE_RESULT);
        Either<String, QfInternalState> currentState = performAction(
                "JOBN 2142 12 750 3 250 800",
                new QfInternalState(false, systemConfig, new HashMap<>(), new HashMap<>())
        );
        NewJobRequest expectedJobRequest = new NewJobRequest(12, 2142, 750L, 3, 250L, 800L);
        assertRight(
                new QfInternalState(false, systemConfig, mapOf(12, expectedJobRequest), mapOf(12, Pair.of("Unavailable but capable", 3))),
                currentState
        );
        verify(cms).scheduleJob(eq(12), eq("Unavailable but capable"), eq(3));
    }

    @Test
    public void testStateShouldShouldReturnFinalStateOnNone() {
        Either<String, QfInternalState> currentState = performActionWithInitialState("NONE");
        assertRight(
                new QfInternalState(true, systemConfig, new HashMap<>(), new HashMap<>()),
                currentState
        );
    }

    @Test
    public void testStateMachineShouldAttemptToMigrateWaitingJobsOnceAJobIsCompleted() {
        NewJobRequest jobAboutToEnd = new NewJobRequest(12, 2142, 750L, 3, 250L, 800L);
        NewJobRequest waitingJob = new NewJobRequest(10, 300, 750L, 3, 250L, 800L);
        ServerStateItem serverHandlingJob = generateServerStateItem(2);
        ServerStateItem serverHandlingWaitingJob = generateServerStateItem(3);
        when(cms.signalRedy()).thenReturn(WRITE_RESULT);
        when(cms.getServerJobStatus(any(), any())).thenReturn(right(list(
                new JobQueryResult(
                        10,
                        1,
                        1,
                        -1,
                        750,
                        3,
                        250L,
                        800
                )
        )));
        when(cms.getServerState(any())).thenReturn(right(list(serverHandlingJob)));
        Map<Integer, NewJobRequest> jobMap = mapOf(12, jobAboutToEnd);
        jobMap.put(10, waitingJob);
        Map<Integer, Pair<String, Integer>> jobServerMap = mapOf(12, serverHandlingJob.toServerKey());
        jobServerMap.put(10, serverHandlingWaitingJob.toServerKey());
        Either<String, QfInternalState> currentState = performAction(
                "JCPL 1 12",
                new QfInternalState(false, systemConfig, jobMap, jobServerMap)
        );
        QfInternalState expectedState = new QfInternalState(false, systemConfig, mapOf(10, waitingJob), mapOf(10, serverHandlingJob.toServerKey()));
        assertRight(expectedState, currentState);
        verify(cms).migrateJob(10, serverHandlingWaitingJob.toServerKey(), serverHandlingJob.toServerKey());
    }

    private Either<String, QfInternalState> performActionWithInitialState(String action) {
        StateMachine<QfInternalState, String> stateMachine = QfFactory.STATE_MACHINE.createStateMachine(cms);
        Either<String, QfInternalState> state = right(new QfInternalState(false, systemConfig, new HashMap<>(), new HashMap<>()));
        return flatMap(state, (s) -> stateMachine.accept(action, s));
    }

    private Either<String, QfInternalState> performAction(String action, QfInternalState state) {
        StateMachine<QfInternalState, String> stateMachine = QfFactory.STATE_MACHINE.createStateMachine(cms);
        return stateMachine.accept(action, state);
    }

}