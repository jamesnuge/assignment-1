package xyz.jamesnuge.scheduling.quickFit;

import java.util.Map;
import java.util.Objects;

import xyz.jamesnuge.Pair;
import xyz.jamesnuge.config.SystemConfig;
import xyz.jamesnuge.messaging.NewJobRequest;
import xyz.jamesnuge.scheduling.State;

public class QfInternalState implements State {

    private final Boolean isFinalState;
    private final SystemConfig config;
    // Map of the job id to the details received from the server
    private final Map<Integer, NewJobRequest> jobMap;
    // Map of the server key (type and id) to the list of job ids as they're assigned to a server
    private final Map<Integer, Pair<String, Integer>> jobServerMap;

    public QfInternalState(final Boolean isFinalState, final SystemConfig config, Map<Integer, NewJobRequest> jobMap, Map<Integer, Pair<String, Integer>> jobServerMap) {
        this.isFinalState = isFinalState;
        this.config = config;
        this.jobMap = jobMap;
        this.jobServerMap = jobServerMap;
    }

    public QfInternalState(final Boolean isFinalState, Map<Integer, NewJobRequest> jobMap, Map<Integer, Pair<String, Integer>> jobServerMap, Boolean isFirst) {
        this(isFinalState, null, jobMap, jobServerMap);
    }

    @Override
    public Boolean isFinalState() {
        return this.isFinalState;
    }

    public SystemConfig getConfig() {
        return config;
    }

    public Map<Integer, NewJobRequest> getJobMap() {
        return jobMap;
    }

    public Map<Integer, Pair<String, Integer>> getJobServerMap() {
        return jobServerMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QfInternalState that = (QfInternalState) o;
        return Objects.equals(isFinalState, that.isFinalState) && Objects.equals(config, that.config) && Objects.equals(jobMap, that.jobMap) && Objects.equals(jobServerMap, that.jobServerMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isFinalState, config, jobMap, jobServerMap);
    }

    @Override
    public String toString() {
        return "QfInternalState{" +
                "isFinalState=" + isFinalState +
                ", config=" + config +
                ", jobMap=" + jobMap +
                ", jobServerMap=" + jobServerMap +
                '}';
    }
}