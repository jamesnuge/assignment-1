package xyz.jamesnuge.scheduling.quickFit;

import fj.Ord;
import fj.data.Either;
import fj.data.List;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import xyz.jamesnuge.MessageParser;
import xyz.jamesnuge.Pair;
import xyz.jamesnuge.SystemInformationUtil;
import xyz.jamesnuge.messaging.*;
import xyz.jamesnuge.scheduling.StateConfigurationFactory;
import xyz.jamesnuge.scheduling.StateMachineFactory;
import xyz.jamesnuge.state.ServerStateItem;

import static fj.data.Either.right;
import static xyz.jamesnuge.MessageParser.Message.OK;
import static xyz.jamesnuge.Util.chain;
import static xyz.jamesnuge.Util.flatMap;

public class QfFactory {

    private static final Logger logger = LogManager.getLogger(QfFactory.class);

    public static final StateMachineFactory<QfInternalState> STATE_MACHINE = (cms) -> (message, currentState) -> {
        if (message.contains(MessageParser.InboudMessage.JOBN.name())) { // New job message
            // Get the details of the job request
            final NewJobRequest jobRequest = NewJobRequest.parseFromJOBNMessage(message);
            // Get the best server to run the job
            final Either<String, ServerStateItem> serverToHandleJobs = getServerToHandleJob(jobRequest, cms);
            // Schedule the job
            final Either<String, ServerStateItem> jobSchedulingResult = scheduleJob(cms, jobRequest, serverToHandleJobs);
            return jobSchedulingResult.rightMap((server) -> {
                // Create new state with job assigned to server
                return updateStateWithScheduledJob(server, jobRequest, currentState);
            });
        } else if (message.contains(MessageParser.InboudMessage.NONE.name())) { // Finished message
            return right(new QfInternalState(true,  currentState.getConfig(), currentState.getJobMap(), currentState.getJobServerMap()));
        } else if (message.contains(MessageParser.InboudMessage.JCPL.name())) { // Job completion message
            final Integer completedJobId = JobCompletionMessageUtil.getJobIdFromJobCompletionMessage(message);
            final Pair<String, Integer> serverRunningCompletedJob = currentState.getJobServerMap().get(completedJobId);
            // Get the waiting jobs from the ds-server and order them by submitTime asc (jobs that were submitted first)
            final List<Pair<Pair<String, Integer>, NewJobRequest>> waitingJobsSortedBySubmittedTime = getWaitingJobs(completedJobId, currentState, cms)
                    .sort(Ord.intOrd.contramap((job) -> job.getValue().submitTime));
            Map<Integer, Pair<String, Integer>> reassignedJobServerMap = new HashMap<>();
            for (final Pair<Pair<String, Integer>, NewJobRequest> waitingJob: waitingJobsSortedBySubmittedTime) {
                // For each job, check if the job could be assigned to the server that just finished running the job
                // and assign it if it can
                if (serverCanAcceptJob(waitingJob, serverRunningCompletedJob, cms)) {
                    final Integer jobToMigrate = waitingJob.getValue().jobId;
                    logger.info("MIGRATING JOB");
                    logger.info("Job id: " + jobToMigrate);
                    logger.info("Original server: " + currentState.getJobServerMap().get(jobToMigrate));
                    logger.info("Server: " + serverRunningCompletedJob);
                    cms.migrateJob(jobToMigrate, waitingJob.getKey(), serverRunningCompletedJob);
                    reassignedJobServerMap.put(jobToMigrate, serverRunningCompletedJob);
                }
            }

            return cms.signalRedy().rightMap((_s) -> {
                Map<Integer, NewJobRequest> jobMap = currentState.getJobMap();
                jobMap.remove(completedJobId);
                Map<Integer, Pair<String, Integer>> jobServerMap = currentState.getJobServerMap();
                jobServerMap.remove(completedJobId);
                jobServerMap.putAll(reassignedJobServerMap);
                return new QfInternalState(false, currentState.getConfig(), jobMap, jobServerMap);
            });
        } else { // Unknown message - Perform NOOP
            return right(currentState);
        }
    };

    private static Either<String, ServerStateItem> scheduleJob(ClientMessagingService cms, NewJobRequest jobRequest, Either<String, ServerStateItem> serverToHandleJobs) {
        return flatMap(
                serverToHandleJobs,
                (serverToHandleJob) -> chain(
                        cms.scheduleJob(jobRequest.jobId, serverToHandleJob.getType(), serverToHandleJob.getId()),
                        (_s) -> cms.getMessage(),
                        (s) -> s.equals(OK.name()) ? cms.signalRedy() : chain(cms.getMessage(), (_s) -> cms.signalRedy())
                ),
                // return the server, so it can be saved in the state
                // Chaining here is a little clunky due to the simple implementation of the 'chain' utility
                (_s) -> serverToHandleJobs
        );
    }

    private static boolean serverCanAcceptJob(Pair<Pair<String, Integer>, NewJobRequest> pair, Pair<String, Integer> freeServer, ClientMessagingService cms) {
        // Performs a GET Avail and checks if the free server is in the returned list
        Either<String, List<ServerStateItem>> eitherServerState = cms.getServerState(ConfigRequest.createAvailTypeConfigRequest(pair.getValue()));
        if (eitherServerState.isRight()) {
            List<ServerStateItem> availableServers = eitherServerState.right().value();
            return availableServers.exists((s) -> s.toServerKey().equals(freeServer));
        }
        return false;
    }

    private static List<Pair<Pair<String, Integer>, NewJobRequest>> getWaitingJobs(Integer jobId, QfInternalState currentState, ClientMessagingService cms) {
        // Get the server of the job that just finished
        Pair<String, Integer> serverKey = currentState.getJobServerMap().get(jobId);
        // Get the list of servers currently processing jobs
        return List.iterableList(currentState.getJobServerMap()
                .values()
                .stream()
                // remove the server that just finished processing the job
                .filter(stringIntegerPair -> !stringIntegerPair.equals(serverKey))
                .flatMap((key) -> {
                    //Query the system for the job status for each server and return the list of waiting jobs
                    Either<String, List<JobQueryResult>> serverJobStatus = cms.getServerJobStatus(key.getKey(), key.getValue());
                    if (serverJobStatus.isRight()) {
                        List<JobQueryResult> jobStatusResults = serverJobStatus.right().value();
                        return jobStatusResults.filter(JobQueryResult::isWaiting).toJavaList().stream();
                    } else {
                        return List.<JobQueryResult>nil().toJavaList().stream();
                    }
                })
                // Map the returned jobs to the server key and original job request
                .map((jobQueryResult) -> Pair.of(
                        currentState.getJobServerMap().get(jobQueryResult.jobId),
                        currentState.getJobMap().get(jobQueryResult.jobId)
                )).collect(Collectors.toList()));
    }

    private static Either<String, ServerStateItem> getServerToHandleJob(NewJobRequest newJobRequest, ClientMessagingService cms) {
        // Get all machines capable of running our job, then order by number of waiting jobs, ascending
        // If no server is available, then get a capable server based on the 'best fit' (closest number of cores)
        final ConfigRequest capableRequest = ConfigRequest.createAvailTypeConfigRequest(newJobRequest);
        return cms.getServerState(capableRequest)
                .rightMap((servers) -> servers.sort(Ord.intOrd.contramap(ServerStateItem::getCores)).sort(Ord.intOrd.contramap(ServerStateItem::getWaitingJobs)))
                .rightMap((list) -> list.headOption().orSome(getCapableServer(cms, newJobRequest)));
    }

    private static ServerStateItem getCapableServer(ClientMessagingService cms, NewJobRequest jobRequest) {
        final List<ServerStateItem> capableServers = cms.getServerState(ConfigRequest.createCapableTypeConfigRequestForJob(jobRequest))
                .right().value();
        return capableServers.sort(Ord.intOrd.contramap(ServerStateItem::getCores)).head();
    }

    public static final StateConfigurationFactory<QfInternalState> CONFIGURATION = (cms) -> SystemInformationUtil
            .loadSystemConfig(".")
            .rightMap((systemConfig) -> new QfInternalState(false, systemConfig, new HashMap<>(), new HashMap<>()));

    private static QfInternalState updateStateWithScheduledJob(ServerStateItem serverToHandleJob, NewJobRequest newJobRequest, QfInternalState currentState) {
        final Map<Integer, NewJobRequest> jobMap = currentState.getJobMap();
        jobMap.put(newJobRequest.jobId, newJobRequest);
        final Map<Integer, Pair<String, Integer>> serverJobsMap = currentState.getJobServerMap();
        serverJobsMap.put(newJobRequest.jobId, serverToHandleJob.toServerKey());
        return new QfInternalState(false, currentState.getConfig(), jobMap, currentState.getJobServerMap());
    }


}
