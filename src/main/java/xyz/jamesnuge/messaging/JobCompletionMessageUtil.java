package xyz.jamesnuge.messaging;

public class JobCompletionMessageUtil {
    private JobCompletionMessageUtil() {}

    public static Integer getJobIdFromJobCompletionMessage(final String rawMessage) {
        return Integer.parseInt(rawMessage.split(" ")[2]);
    }
}
