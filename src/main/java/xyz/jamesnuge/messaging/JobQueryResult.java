package xyz.jamesnuge.messaging;

import java.util.Objects;

public class JobQueryResult {

    private static final Integer WAITING_JOB_STATE = 1;
    private static final Integer ACTIVE_JOB_STATE = 2;

    public final Integer jobId;
    // 1 = waiting
    // 2 = active
    public final Integer jobState;
    public final Integer submitTime;
    public final Integer startTime;
    public final Integer estimatedRunTime;
    public final Integer core;
    public final Long memory;
    public final Integer disk;

    public static JobQueryResult parseFromString(String raw) {
        final String[] individualItems = raw.split(" ");
        return new JobQueryResult(
                Integer.parseInt(individualItems[0]),
                Integer.parseInt(individualItems[1]),
                Integer.parseInt(individualItems[2]),
                Integer.parseInt(individualItems[3]),
                Integer.parseInt(individualItems[4]),
                Integer.parseInt(individualItems[5]),
                Long.parseLong(individualItems[6]),
                Integer.parseInt(individualItems[7])
        );
    }

    public JobQueryResult(Integer jobId, Integer jobState, Integer submitTime, Integer startTime, Integer estimatedRunTime, Integer core, Long memory, Integer disk) {
        this.jobId = jobId;
        this.jobState = jobState;
        this.submitTime = submitTime;
        this.startTime = startTime;
        this.estimatedRunTime = estimatedRunTime;
        this.core = core;
        this.memory = memory;
        this.disk = disk;
    }

    public Boolean isActive() {
        return Objects.equals(this.jobState, ACTIVE_JOB_STATE);
    }

    public Boolean isWaiting() {
        return Objects.equals(this.jobState, WAITING_JOB_STATE);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobQueryResult that = (JobQueryResult) o;
        return Objects.equals(jobId, that.jobId) && Objects.equals(jobState, that.jobState) && Objects.equals(submitTime, that.submitTime) && Objects.equals(startTime, that.startTime) && Objects.equals(estimatedRunTime, that.estimatedRunTime) && Objects.equals(core, that.core) && Objects.equals(memory, that.memory) && Objects.equals(disk, that.disk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobId, jobState, submitTime, startTime, estimatedRunTime, core, memory, disk);
    }

    @Override
    public String toString() {
        return "JobQueryResult{" +
                "jobId=" + jobId +
                ", jobState=" + jobState +
                ", submitTime=" + submitTime +
                ", startTime=" + startTime +
                ", estimatedRunTime=" + estimatedRunTime +
                ", core=" + core +
                ", memory=" + memory +
                ", disk=" + disk +
                '}';
    }
}